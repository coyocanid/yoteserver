package Yote::Server::App;

use strict;
use warnings;

use Data::ObjectStore;
use base 'Data::ObjectStore::Container';

use Digest::MD5;
use File::Copy qw(copy);
use File::Path qw(make_path);
use Email::Valid;
use Scalar::Util qw(openhandle);

our $IMAGES;

BEGIN {
    eval {
        require Image::Magick;
        $Yote::Server::App::IMAGES = 1;
    };
}

sub _init {
    my $self = shift;
    $self->set__logins({});
    $self->set__emails({});
    $self->set__sessions({});
    my $default_session = $self->store->create_container( 'Yote::Server::Session', { _session_id => 0 } );
    $self->set__default_session( $default_session );
    
} #_init

sub _load {
    my $self = shift;
}

#
# Override to make the new user something more than a generic container.
#
sub _newuser {
    my $self = shift;
    $self->store->create_container(@_);
}


sub _rand_path {
    # assume level 4, so 42 billion possiblities
    join('/',map { sprintf( '%x', 256*rand) } (0..3));
} #_rand_path


# --------------- util methods ---------------------------------

#
# callable methods may not start with an uppercase or underscore
#

sub CFG {
    my( $self, $cfg ) = @_;
    $self->vol('CONFIG')->{$cfg};
}

sub UPLOAD_FILE {
    my( $self, $args, $param, $dest_dir ) = @_;

    # a file is represented in the params as a pair
    my $val = $args->{$param};
    return unless ref($val) eq 'ARRAY';

    my $fh = openhandle( $val->[0] );
    if( $fh ) {
        #TODO - check extension validity. dont trust it
        my( $ext ) = ( $val->[1] =~ /(\.[^\.]+)$/ ); 
        my $file = $self->store->create_container( {
            app            => $self,
            extension      => $ext,
            _original_name => $val->[1],
        } );
        my $upload_dir = $dest_dir || $self->CFG('UPLOAD_DIR');
        # TODO - don't put them all in one location
        my $rand_path = $self->_rand_path();
        make_path( "$upload_dir/$rand_path", { error =>  \my $err } );
        if( $err && @$err ) {
            die "Unable to create directory for file [".join(',',@$err)."]";
        }
        my $dest = "$upload_dir/$rand_path/$file$ext";
        copy( $val->[0], $dest );
        $file->set__origin_file( $dest );
        return $file;
    }
    return undef;
} #upload_file

sub UPLOAD_IMAGE {
    unless( $Yote::Server::App::IMAGES ) {
        warn "Unable to load Image::Magick. UPLOAD_IMAGE returning undef. $@";
        return undef;
    }
    my( $self, $args, $param, $dest_dir ) = @_;
    my $val = $args->{$param};

    return unless ref($val) eq 'ARRAY';

    my $fh = openhandle( $val->[1] );
    if( $fh ) {
        my $im = new Image::Magick;
        my( $w, $h, $sz, $type ) = $im->ping( file => $fh );
        if( $type !~ /^(gif|jpeg|png|jpg)$/i ) {
            warn "Unsupported image type '$type'";
            return undef;
        }
        my $ext = lc($type);
        my $upload_dir = $dest_dir || $self->CFG('UPLOAD_DIR');
        my $rand_path = $self->_rand_path();
        make_path( "$upload_dir/$rand_path", { error =>  \my $err } );

        if( $err && @$err ) {
            my $isRoot = $< == 0;
            (my $defuser, undef, undef, my $gid ) = getpwuid $<;
            die "Unable to create directory '$upload_dir/$rand_path' for image [".join(',',@$err)."]";
        }
        my $img = $self->store->create_container( 'Yote::Server::Image', {
            base_dir       => $upload_dir,
            path           => $rand_path,
            
            type           => $type,
            extension      => $ext,
            original_name => $val->[0],
        } );
        my $dest = "$upload_dir/$rand_path/$img.$ext";
        # open my $out_file, '>', $dest;
        # while ( my $bytesread = $fh->read(my $buffer,1024) ) {
        #     print $out_file $buffer;
        # }
        # close $out_file;
        # close $fh;
        copy( $val->[2], $dest );

        $img->set_filename( "$img.$ext" );
        return $img;
    }

    return undef;
} #UPLOAD_IMAGE


# --------------- callable methods -----------------

sub create_account {
    my( $self, $args, $session ) = @_;
    my( $un, $em, $pw1, $pw2 ) = @$args{'un','em','pw','pw2'};

    my $store = $self->store;
    $store->lock( "__EMAILS", "__LOGINS", "__SESSIONS" );

    my $logins = $self->get__logins;
    my $emails = $self->get__emails;

    my $err;

    if ( $un !~ /^\w+$/ ) {
        $err = 'Invalid username';
    }
    elsif ( $pw1 ne $pw2 ) {
        $err = 'passwords dont match';
    }
    elsif ( length( $pw1 ) < 8 ) {
        $err = 'password is too short. Must be at least 8 characters.';
    }
    else {
        eval {
            if ( ! Email::Valid->address( -address => $em, -tldcheck => 1 ) ) {
                $err = 'unable to verify email.';
            }
        };
        warn $@;
        undef $@;
    }

    my $user = $self->REGISTER_ACCOUNT( $un, $pw1, $em );
    unless( $user ) {
        $err = 'Email or username already taken';
    } 

    if( $err ) {
        $session->err( $err );
        return 0;
    }
    
    $self->_create_session( $user, $session );

    $session->ret($user);
    $session->msg( "created account '$un' with email '$em'" );

    $user;
} #create_account

sub REGISTER_ACCOUNT {
    my( $self, $un, $pw, $em, $isAdmin ) = @_;

    my $logins = $self->get__logins;
    my $emails = $self->get__emails;

    return if $logins->{lc($un)} || $emails->{lc($em)};

    my $hash_pw = $self->_hash_pw( $un, $pw );
    my $user = $self->_newuser( {
        _email     => $em,
        login      => $un,
        _hashpass  => $hash_pw,
        app        => $self,
        created    => time,
    } );
    if( $isAdmin ) {
        $user->set__is_admin( $isAdmin );
    }
    $logins->{lc($un)} = $user;
    if( $em ) {
        $emails->{lc($em)} = $user;
    }
    $user;
} #REGISTER_ACCOUNT

sub _hash_pw {
    my( $self, $un, $pw ) = @_;
    crypt( $pw, length( $pw ) . Digest::MD5::md5_hex($un) );
}

sub login {
    my( $self, $args, $session ) = @_;

    my( $unem, $pw ) = @$args{'unorem','pw'};        

    my $user;
    if( $unem =~/\@/ ) {
        my $emails = $self->get__emails;
        $user = $emails->{lc($unem)};
    } else {
        my $logins = $self->get__logins;
        $user = $logins->{lc($unem)};
    }

    # session is used in the following line to prevent a timing oracle
    my $hash_pw = $self->_hash_pw( $user ? $user->get_login : $unem, $pw );
    if( $user ) {
        if( $user->get__hashpass eq $hash_pw ) {
            $session->ret($user);
            $session->msg( 'logged in as '.$user->get_login);
            $self->store->lock( "__SESSIONS" );
            $self->_create_session( $user, $session );
            return 1;
        }
    }
    $session->err( 'incorrect login' );

    return 0;
    
} #login 

sub logout {
    my( $self, $args, $session, $user ) = @_;

    if( $user ) {
        $self->store->lock( "__SESSIONS" );
        my $sessions = $self->get__sessions;
        $user->remove_from__sessions( $session );
        delete $sessions->{$session->get__session_id};
        $session->msg( 'logged out' );
        $session->vol( 'session', $self->get__default_session );

        return 1;
    }
    $session->err(  'not logged in' );
    return 0;
} #logout

sub _create_session {
    my( $self, $user, $session ) = @_;

    my $sessions = $self->get__sessions;

    my $found;
    my $tries = 0;
    my $sess_id;
    until( $found ) {
        $sess_id = int(rand(2**64)); # if this is returning the same number again and again, got huge problems
        $found = ! $sessions->{$sess_id};
        die "Error picking session number" if $tries++ > 10;
    }

    my $sess = $self->store->create_container( 'Yote::Server::Session', {
        _user              => $user,
        _session_id        => $sess_id,
        _session_start     => time,
    } );
    $session->vol( 'session', $sess );
    $sessions->{$sess_id} = $sess;
    $user->add_to__sessions( $sess );
} #_create_session

1;
