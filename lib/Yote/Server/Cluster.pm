package Yote::Server::Cluster;

use strict;
use warnings;

use Data::ObjectStore;
use base 'Data::ObjectStore';

sub handle_cgi {
    my( $cls, $cfg ) = @_;

    my $q = new CGI;

    # 
    # There are types of inputs
    #   * url params - view inputs
    #   * POST multipart/form-data (default, I think, but has file upload too)
    #   * POST application/x-www-form-urlencoded (sending images other ways than file upload)
    #   * POST text/json
    #
    # and types of outputs
    #
    
    my $view_params = { map { $_ => $q->url_param($_) } $q->url_param };
    my $view_path   = delete $view_params->{path};

    my( @qparams ) = ($q->param);

    my $action_params = { map { $_ => scalar $q->param($_) } @qparams };

    my $return_type = $ENV{HTTP_ACCEPT} =~ /\/json/ ? 'json' : 'html';

    my( $new_session_id, $type, $code, $resp ) = 
        $cls->handle( $cfg,
                      $view_path, 
                      $view_params, 
                      $action_params,
                      $return_type,
                    );

    print $q->header( -type   => $type,
                      -status => $code,
                    );
    print $resp;
} #handle_cgi

sub handle {
    my( $cls, $cfg, $view_path, $view_params, $action_params, $return_type ) = @_;

    my $request = ''; 

    # ------------ get the stores -------------
    my $store_root = $cfg->{STORE};

    my $shared_store = $cls->open_store("$store_root/SHARED");
    my $local_store = Data::ObjectStore->open_store("$store_root/LOCAL");


    my $resp = '';

    return ( 'text/html', 
             '200 OK', 
             $resp );
    
} #handle

1;
