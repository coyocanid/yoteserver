package Yote::Server::RecordStore;

use strict;
use warnings;

use Data::RecordStore;

sub open_socket {
    my( $cls, $dir, $args ) = @_;

    my $rec_store = Data::RecordStore->open_store( $dir );


#    my $client = IO::Socket::SSL->new(PeerHost=>"localhost:443",SSL_fingerprint=>"sha256\$d83edeca0cba0adee3e0cfd3ab4d0338f71f9943c08014cbd1d01760fb72fb72") or die "error=$!, ssl_error=$SSL_ERROR";
#   keep alive sockets maybe?

    if( $args->{cluster} ) {
        
        my $host = $args->{host}; #host:port

        my $cluster = [];
        my $bucket;
        # cluster is an ordered list of hostname, port 
        my $clu = $args->{cluster}; # [ [ ], [ ], ]
        for my $buck (0..$#$clu) {
            my $cluname = $clu->[$buck];
            if( $cluname eq $host ) {
                $cluster->[$buck] = 'SELF';
                $bucket = $buck;
            } else {
                $cluster->[$buck] = {
                    PeerHost => $cluname,
                    socket => IO::Socket::SSL->new(PeerHost=>$cluname,SSL_fingerprint=>'sha256$d83edeca0cba0adee3e0cfd3ab4d0338f71f9943c08014cbd1d01760fb72fb72'),
                  };
            }
        }

        return bless {
            store   => $rec_store,
            cluster => $cluster,
        }, $cls;
    }

    return $rec_store;
}

sub next_id {
    my( $self ) = @_;
    my $lastid = $self->store->entry_count;
    
} #next_id


sub stow {
    my( $self, $data, $id ) = @_;
} #stow

sub fetch {
    my( $self, $id ) = @_;
} #fetch

sub delete_record {
    my( $self, $id ) = @_;
} #delete_record

sub use_transaction {
    my $self = shift;
} #use_transaction


1;

__END__
