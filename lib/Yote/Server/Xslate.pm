package Yote::Server::Xslate;
use strict;
use warnings;

use Text::Xslate;

sub new {
    my( $cls, %args ) = @_;
    my $launch_template = delete $args{launch_template};
    bless {
        xslate   => new Text::Xslate( %args ),
        launcher => "$launch_template.tx",
    }, $cls;
}

sub render {
    my( $self, $template, $formatter, $context ) = @_;
    # TODO - decide context, params, utils, etc
    $self->{xslate}->render( $self->{launcher}, { c => $context,
                                                  f => $formatter,
                                                  v => $template,
                                              } );
}

1;
