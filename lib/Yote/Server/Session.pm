package Yote::Server::Session;

use strict;
use warnings;

use Data::ObjectStore;
use base 'Data::ObjectStore::Container';

sub _init {
    my $self = shift;
} #_init

sub reset {
    my $self = shift;
    $self->clearvols( qw( err msg ret ) );
}

sub err {
    shift->vol( 'err', shift );
}

sub msg {
    shift->vol( 'msg', shift );
}

sub ret {
    shift->vol( 'ret', shift );
}

1;
