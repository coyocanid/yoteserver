package Yote::Server::File;

use strict;
use warnings;

use File::Copy;

use Data::ObjectStore;
use base 'Data::ObjectStore::Container';

sub new {
    bless {}, shift;
}

sub src {
    my $self = shift;
    my $of = $self->get__origin_file;
    my $download_path = $self->get_app->CFG('DOWNLOAD_PATH');
    $of =~ s!$download_path!!;
    $of;
}

sub mv {
    my( $self, $dest_dir ) = @_;
    move( $self->__get_origin_file, $dest_dir ) or die "$!";
    $self->set__origin_file( "$dest_dir/$self.".$self->get_extension );
}

1;
