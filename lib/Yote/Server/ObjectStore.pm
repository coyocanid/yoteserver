package Yote::Server::ObjectStore;

use strict;
use warnings;

use Data::ObjectStore;
use Yote::Server::RecordStore;

use base 'Data::ObjectStore';

sub open_server {
    my( $cls, $dir, $opts ) = @_;


    if( $opts->{servers} ) {
        my $store = $cls->open_store( $dir );
        $store->[0] = Yote::Server::RecordStore->open_socket( $dir, $opts );
        return $store;
    }

    Data::ObjectStore->open_store( $dir );

} #open_server

1;
