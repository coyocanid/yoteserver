package Yote::Server::AppManager;

#
# A repo for apps. Have to evaluate if this
# is a necessary class. Might not be, as it is
# too simple.
#

use strict;
use warnings;
no warnings 'uninitialized';

use Yote::Server::Session;

use Data::ObjectStore;
use base 'Data::ObjectStore::Container';

sub _init {
    my $self = shift;
    $self->set_apps( {} );
}

sub fetch_app {
    my( $self, $cfg ) = @_;
    
    my( $app_handle, $app_class ) = @$cfg{'APP_TAG','APP_CLASS'};
    my $app_key = $app_handle ? "$app_class^$app_handle" : $app_class;
    my $apps = $self->get_apps;
    my $app = $apps->{$app_key} //= $self->store->create_container( $app_class, {
        _default_session => $self->store->create_container( 'Yote::Server::Session' ),
    } );
    $app->vol('CONFIG',$cfg);
    $app;
} #fetch_app

1;

__END__
