package Yote::Server::AppBuilder;

use strict;
use warnings;

use Cwd;
use File::Copy;
use File::Path qw(make_path);
use Term::ReadKey;

use Yote::Server;

my $isRoot = $< == 0;
(my $defuser, undef, undef, my $gid ) = getpwuid $<;
my $defgroup = getgrgid($gid);
my( $seluser, $selgroup );
#
# Want a user/group for
#   store, since it will be updated as used
#   class files and programs
#
#

# asks the question. uses the default if no answer
# given. If no answer nor default, asks until it gets
# an answer.
sub ask {
    my( $self, $q, $def, $secretchar ) = @_;

    print "$q".($def?" $def":"").">";
    my $ans;
    until( $ans ) {
        if( $secretchar ) {
            $ans = $self->getpwinput( $secretchar );
        } else {
            $ans = $self->getinput // $def;
        }
        unless( $ans ) {
            print "A value must be entered. Use Ctrl-C to cancel.\n";
        }
    }
    $ans;
} #ask

sub getpwinput {
    my( $self, $sec ) = @_;

    ReadMode('cbreak');
    my $pass = '';

    while (1) {
        my $c;
        1 until defined($c = ReadKey(-1));
        if( $c eq "\n" ) {
            print "\n";
            last;
        } elsif( ord($c) == 127 || ord($c) == 8 ) {
            if( length($pass) > 0 ) {
                chop $pass;
                print "\b \b";
            }
        } else {
            print $sec;
            $pass .= $c;
        }
    }

    ReadMode('restore');
    return $pass;
}

sub getinput {
    my $ans = <STDIN>;
    chomp $ans;
    $ans eq '' ? undef : $ans;
}

sub askdir {
    my( $self, $msg, $default, $ask ) = @_;
  DIRAGAIN:
    my $dir = $self->ask( $msg, $default );
    unless ( $self->ensure_dir( $dir,
                                $ask ? $self->ask( "User to own $dir", $seluser ) : $seluser,
                                $ask ? $self->ask( "Group to own $dir", $selgroup ) : $selgroup,
                                775 ) )
      {
          goto DIRAGAIN;
      }
    $dir;
} #askdir

sub try_dir {
    my( $self, $dir, @res ) = @_;
    $self->ensure_dir( $dir, @res ) || die "Unable to create $dir";
}

# makes sure the directory exists with the
# given user, group and mode
sub ensure_dir {
    my( $self, $dir, $user, $group, $mode ) = @_;

    if( ! -e $dir ) {
        my $errs;
        my $args = { error => \$errs };
        if( $user && $user ne $defuser ) {
            $args->{user}  = $user;
        }
        if( $group && $group ne $defgroup ) {
            $args->{group} = $group;
        }
        if( $mode ) {
            $args->{mode}  = oct($mode);
        }
print STDERR Data::Dumper->Dump([$dir,$args,"MAKEADIR"]);
        make_path( $dir, $args );
;
        if( $errs && @$errs ) {
            print "Got errors creating directory ".join(",",map{ values %$_ } @$errs)."\n";
            return 0;
        }
    }
    # check user and group and mode
    my( $dev,$ino,$curmode,$nlink,$uid,$gid ) = stat $dir;
    my $curowner = getpwuid( $uid );
    if( $user && $curowner ne $user ) {
        print "Was not able to change owner of '$dir' from '$curowner' to '$user'\n";
        return 0;
    }
    my $curgroup = getgrgid( $gid );
    if( $group && $curgroup ne $group ) {
        print "Was not able to change group of '$dir' from '$curgroup' to '$group'\n";
        return 0;
    }
    $curmode = sprintf( "%04o", $curmode & 07777 );
    if( $mode && $curmode != $mode) {
        warn "Was not able to change mode of '$dir' from '$curmode' to '$mode'";
    }

    return 1;
} #ensure_dir

sub fileout {
    my( $self, $file, $mode, $str ) = @_;
    open my $out, '>', $file;
    print $out $str;
    close $out;
    chmod oct($mode), $file;
    ( undef,undef,my $curmode ) = stat $file;
    $curmode = sprintf( "%04o", $curmode & 07777 );
    if( $mode && $curmode != $mode ) {
        warn "Was not able to change mode of '$file' from '$curmode' to '$mode'\n";
    }
} #fileout

sub build {
    my $self = shift;
    $self = ref($self) ? $self : bless( {}, ($self || __PACKAGE__ ) );

    my $apptag = $self->ask("Shorthand name for app");

    my $defdir = cwd()."/$apptag";
  PICKAPPDIR:
    my $usedir   = $self->ask("Directory to use for app", $defdir );
    my( $basedir ) = ( $usedir =~ m!(.*)/[^/]+/?$! );
    if ( -e $usedir ) {
        if ( $self->ask( "This directory already exists. Still use?", "Y" ) !~ /^[yY]/ ) {
            print "Exiting $0\n";
            return;
        }
        my( $dev,$ino,$curmode,$nlink,$uid,$gid ) = stat $usedir;
        $seluser = getpwuid( $uid );
        $selgroup = getgrgid( $gid );
    } else {
        $seluser  = $self->ask("User to own the app", $defuser );
        $selgroup = $self->ask("Group to own the app", $defgroup );
    }
    $self->ensure_dir( $usedir, $seluser, $selgroup, 775 ) || goto PICKAPPDIR;

  GETCLS:
    my $cls = $self->ask("App Class",ucfirst($apptag));
    my( @clsparts ) = split( /::/, $cls );
    if( $cls && 0 < grep { $_ !~ /^[a-zA-Z_][a-zA-Z0-9_]*$/ } @clsparts ) {
        print "The class must be in the form of a perl package, like My::Foo::Bar\n";
        goto GETCLS;
    }

    my( $classfile, @clspath ) = reverse @clsparts;
    @clspath = reverse @clspath;

    my $objstore_default = "$usedir/objstore";
    my $objstoredir = $self->askdir("Datastore for $apptag", $objstore_default, 'askusergroup' );
    my $uploaddir   = $self->askdir("Directory for user uploads", "$usedir/upload" );
    my $htmldir     = $self->askdir("Directory for html", "$usedir/html" );
    my $cssdir      = $self->askdir("Directory for css", "$htmldir/css" );
    my $jsdir       = $self->askdir("Directory for html", "$htmldir/js" );
    my $downloaddir = $self->askdir("Directory for user downloads", "$htmldir/download" );
    my $imgdir      = $self->askdir("Directory for user image uploads", $downloaddir );
    my $templatedir = $self->askdir("Directory for app templates", "$usedir/templates/$apptag" );

    $self->try_dir( "$usedir/cgi" );
    $self->try_dir( "$usedir/modperl" );
    $self->try_dir( $templatedir );

    my $launcher = $self->ask("Template to launch first", "$templatedir/launcher" );
    $launcher =~ s/\.tx$//;

    $self->fileout( "$usedir/templates/$launcher.tx", 644, <<'LEND' );
<:
 # purpose: 
 #    This provides macros and loads the main template. It is a place to add
 #    more macros.
 #
 #    Text::Xslate does not currently provide for importing of macro files. 
 #    Macros _can_ be passed to included templates. To call them in included
 #    templates, they must be extracted from any hash container they are in.
 #
 macro incl -> ($tmpl,$template_params) {
   include $tmpl { _ => $_, 
                   c => $_.c,
                   m => $_.m,
                   f => $_.f,
                   p => $template_params }
 }
 my $macs = {
   incl => incl,
 };
 include 'main.tx' { _ => $_, c => $_.c, m => $macs, f => $_.f }
:>
LEND

    $self->fileout( "$cssdir/main.css", 644, <<'MEND' );
// app-wide css file
MEND

    $self->fileout( "$jsdir/main.js", 644, <<'JEND' );
// write your javascript here
JEND

    $self->fileout( "$usedir/templates/frame.tx", 644, <<'FREND' );
<:
 # purpose: 
 #    This template provides a frame for placing all templates.
 #
:><!DOCTYPE html>
<html>
  <head>
  </head>
  <body>
  </body>
</html>
FREND


    
    $self->try_dir( join('/', $usedir, 'lib', @clspath) );
    my $clspath = join('/', $usedir, 'lib', @clspath, "$classfile.pm" );
    $self->fileout( $clspath, 664, <<CEND );
package $cls;

use strict;
use warnings;

use Yote::Server::App;
use base 'Yote::Server::App';

#
# Methods that begin with a lower case letter
# are callable by web requests. 
#
# The following methods are defined in the base class
# and may be overridden :
#
#   * create_account( { un => 'username', em => 'email', pw => 'password', pw2 => 'password repeated' }, \$session )
#   * login( { unorem => 'username or email', pw => 'password' }, \$session)
#   * logout( {}, \$session, \$user )
#

1;
CEND

    my $cfg = {
        APP_TAG         => $apptag,
        APP_CLASS       => $cls,
        BASE_DIR        => $usedir,
        DOWNLOAD_DIR    => $downloaddir,
        HTML_DIR        => $htmldir,
        CSS_DIR         => $cssdir,
        JS_DIR          => $jsdir,
        IMAGE_DIR       => $imgdir,
        OBJECT_STORE    => $objstoredir,
        TEMPLATE        => $templatedir,
        LAUNCH_TEMPLATE => $launcher,
        UPLOAD_DIR      => $uploaddir,
    };
    my $cfg_string = "my \$cfg = {\n\t" .
        join( "\n\t", map { "$_ => '$cfg->{$_}'," } sort keys %$cfg ) .
        "\n};\n";

    # create cgi
    my $cgi_def = "$usedir/cgi/$apptag.cgi";
    $self->fileout( $cgi_def, 775, <<IEND );
#!/usr/bin/perl

use strict;
use warnings;

use lib "$usedir/lib";
use $cls;
use Yote::Server;

$cfg_string

Yote::Server::handle_cgi( \$cfg );
IEND


    # create modperl
    my $modperl_def = "$usedir/modperl/$apptag.pm";
    $self->fileout( $modperl_def, 664, <<DEND );
package $apptag;
use strict;
use warnings;

use lib "$usedir/lib";
use $cls;
use Yote::Server;

$cfg_string

sub handler {

  Yote::Server::handle_modperl( shift, \$cfg );
} #handler

1;

DEND

    # ask to copy cgi
    my $cgi_loc = $self->ask( "Location for cgi '$apptag.cgi'", $cgi_def );
    if( $cgi_loc ne $cgi_def ) {
      copy( $cgi_def, $cgi_loc );
    }

    # ask to copy modperl
    my $modperl_loc = $self->ask( "Location for modperl '$apptag.pm'", $modperl_def );
    if( $modperl_loc ne $modperl_def ) {
      copy( $modperl_def, $modperl_loc );
    }

    # how about an admin account
    my $admin_name = $self->ask( "Name of admin account", 'yote' );
  ASKPW:
    my $admin_pw = $self->ask( "admin password", '', '*' );
    my $admin_pw_2 = $self->ask( "repeat admin password", '', '*' );
    if( !$admin_pw || $admin_pw ne $admin_pw_2 ) {
       goto ASKPW;
    }

    #initialize store
    my $store = Yote::Server->open_store( $objstoredir );
    my $root = $store->load_root_container;
    
    my $appman = $root->get_app_manager;
    unless( $appman ) {
        $appman = $store->create_container( 'Yote::Server::AppManager' );
        $root->set_app_manager( $appman );
    }
    my $app = $appman->fetch_app( $cfg );
    my $admin = $appman->REGISTER_ACCOUNT( $admin_name, $admin_pw, undef, 'is_admin' );

    print "App '$apptag' built\n";
} #build


1;

__END__

=head1 NAME

 Data::Server::AppBuilder

=head1 SYNPOSIS

 $ # run directly
 $ perl /usr/local/yote-buildapp

 # run from a program
 use Data::Server::AppBuilder;
 Data::Server::AppBuilder::build;

=head1 DESCRIPTION

 This builds a new app skeleton by asking questions.

 The components of the app are as follows :

    app directory
    app configuration
    object store
    cgi
    modperl
    lib path
    class module
    upload directory
    download directory
    image directory
    template directory

 The app may have its own object store or use an
 object store that serves other apps or other purposes.

 This design allows the app to be stand alone or part
 of a system wide architecture.

=head1 AUTHOR

 Eric Wolf
 coyocanid@gmail.com

=head1 COPYRIGHT AND LICENSE

 Copyright (c) 2019 Eric Wolf. All rights reserved.
 This program is free software; you can redistribute it and/or modify it
 under the same terms as Perl itself.

=cut
