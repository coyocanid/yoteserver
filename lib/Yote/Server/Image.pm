package Yote::Server::Image;

use strict;
use warnings;

use Yote::Server::File;
use base 'Yote::Server::File';

sub filepath {
    my $self = shift;
    $self->get_path .'/'. $self->get_filename;
}

sub size {
    my( $self, $w, $h ) = @_;
    my $orf = $self->get__origin_file;
    my $of = $orf;
    my $ext = $self->get_extension;
    $of =~ s/\.$ext$/_${w}_${h}.$ext/;

    unless( -e $of ) {
        # use Image::Magick for this
        `convert $orf -resize ${w}x$h $of`;
    }

    $self->add_to__resized( $of );
    
    $of =~ s!/var/www/html!!; #NO!
    $of;
}

sub _destroy {
    my $self = shift;
    # TODO - more safety here
    unlink $self->get__origin_file;
    for my $of (@{$self->get__resized}) {
        unlink $of;
    }
}


1;

__END__
