package Yote::Server;

use strict;
use warnings;
no warnings 'uninitialized';

# non-core perl
use Apache2::Const -compile => 'OK';
use CGI::Cookie;
use CGI;
use JSON;

# core perl
use File::Path qw(make_path);
use Data::Dumper;

# yote and Data::ObjectStore/Data::RecordStore
use lib '/home/wolf/opensource/recordstore/lib';
use lib '/home/wolf/opensource/objectstore/lib';
use Data::ObjectStore;
use Yote::Server::Formatter;
use Yote::Server::AppManager;


use vars qw($VERSION);

$VERSION = '2';


# Package variables set on init.
our %cache;
our $formatter;

#
# Override to use something other than Xslate
#
sub _renderer {
    my $cls = shift;
    require Yote::Server::Xslate;
    return Yote::Server::Xslate->new( @_ );
}

#
# As a sub so that subclasses can override the
# store..maybe have a clustered store, for example.
#
sub open_store {
    my( $cls, $storestring ) = @_;
    Data::ObjectStore->open_store( $storestring );
} #open_store

sub init {
    # init makes sure there is an app manager
    # _and_ the app in question
    # and to do this, it has to be aware of configuration
    my( $cls, $cfg ) = @_;

    my $store_root_dir = $cfg->{OBJECT_STORE};
    my $store = $cache{$store_root_dir} //= $cls->open_store( $store_root_dir );

    my $root = $store->load_root_container;
    my $appman = $root->get_app_manager;
    
    unless( $appman ) {
        $appman = $store->create_container( 'Yote::Server::AppManager' );
        $root->set_app_manager( $appman );
    }

    my $template_dir = $cfg->{TEMPLATE};
    my $renderer = $cache{$template_dir} //= $cls->_renderer(
        path              => $template_dir,
        input_layer       => ':utf8',
        launch_template   => $cfg->{LAUNCH_TEMPLATE},
      );
    $formatter //= new Yote::Server::Formatter;
    
    my $app = $appman->fetch_app( $cfg );

    $store->save;

    $app, $renderer;
} #init

sub handle_cgi {
    my( $cls, $cfg ) = @_;

    my $q = new CGI;

    # 
    # There are types of inputs
    #   * url params - view inputs
    #   * POST multipart/form-data (default, I think, but has file upload too)
    #   * POST application/x-www-form-urlencoded (sending images other ways than file upload)
    #   * POST text/json
    #
    # and types of outputs
    #
    
    my $session_id = $q->cookie( 'session' );

    my $view_params = { map { $_ => $q->url_param($_) } $q->url_param };
    my $view_path   = delete $view_params->{path};

    my( @qparams ) = ($q->param);

    my $action_params = { map { my( $val, $up ) = ( scalar $q->param($_), scalar $q->upload($_) );
                                $_ => $up ? ["$val",$up,$q->tmpFileName($up)] : $val  } @qparams };

    my $return_type = $ENV{HTTP_ACCEPT} =~ /\/json/ ? 'json' : 'html';

    my( $new_session_id, $type, $code, $resp ) = 
        $cls->handle( $cfg,
                      $session_id,
                      $view_path, 
                      $view_params, 
                      $action_params,
                      $return_type,
                    );

    my $cookie = $q->cookie( -name   => 'session', 
                             -value  => $new_session_id,
                             #-secure => 1, #eventually
                           );
    print $q->header( -cookie => $cookie,
                      -type   => $type,
                      -status => $code,
                    );
    print $resp;

} #handle_cgi

sub handle_modperl {
    my( $cls, $r, $cfg ) = @_;

    my %cookies = CGI::Cookie->fetch;
    my $session_id = $cookies{session};

    my $view_params = $r->args;

    my $view_path   = delete $view_params->{path};
    
    my $action_params = $r->content;

    my( $new_session_id, $type, $code, $resp ) = $cls->handle( $cfg,
                                                               $session_id,
                                                               $view_path, 
                                                               $view_params, 
                                                               $action_params,
                                                             );

    my $cookie = CGI::Cookie->new( -name   => 'session', 
                                   -value  => $new_session_id,
                                   #-secure => 1, #eventually
                                 );
    $r->headers_out->add( 'Set-Cookie', $cookie );
    print $r->header( -cookie => $cookie,
                      -type   => $type,
                      -status => $code,
                    );
    print $resp;
    
    return 0;
} #handle_modperl

sub handle {
    my( $cls, $cfg, $session_id, $view_path, $view_params, $action_params, $return_type ) = @_;

    my( $app, $renderer ) = $cls->init($cfg);

    my $session;
    if( $session_id ) {
        $session = $app->get__sessions({})->{$session_id};
    }
    unless( $session ) {
        $session = $app->get__default_session;
    }

    $session->vol( 'session', $session );

    my $is_json = $return_type eq 'json';

    my( $resp, $suc );

    my $method = $action_params->{method};

    if ( $method && $method =~ /^[a-z]/ && $app->can($method) ) {
        $session->reset;
        
        eval {
            # break from the more functional paradigm of return values
            # and have the method pack the return value in
            # the session as a side effect
            $suc = $app->$method( $action_params, $session, $session->get__user );
            $session = $session->vol( 'session' );
            $app->store->save;

            if ( $is_json ) {

                #
                # got to unpack the response due to blessed things?
                #
                # think about creating an account
                #  (HTTP)
                #   - link to 'create' clicked on
                #   - user enters info, submits form
                #   - action fail?
                #   -   suc - 0 (fail)
                #   -   session->{err} - err message
                #   -   back to same template which
                #   -   reports the error and is given
                #            * res
                #            * err
                #            * msg
                #   - action success?
                #   -   suc - 1 (success)
                #   -   session->{ret} - return value
                #   -   session->{msg} - result message
                #   -   back to same template, which 
                #      may be
                #
                #

                $resp = {
                    suc => $suc,
                    ret => $session->vol( 'ret' ),
                    err => $session->vol( 'err' ),
                    msg => $session->vol( 'msg' ),
                };
            }
        };
        if ( $@ ) {
            print STDERR "ERR $@\n";
            $session->err( "Something went wrong");
            if ( $is_json ) {
                $resp = {
                    err => "Something went wrong",
                };
            }
        }
    } # has method
    elsif ( $is_json ) {
        $resp = {
            err => 'target not found',
        };
    }

    $app->store->unlock;

    if( $is_json ) {
        return ( $session->vol( 'session_id' ),
                 'application/json', 
                 '200 OK', 
                 to_json( $resp ) );
    }
    if( ! -e "$cfg->{TEMPLATE}/$view_path.tx" ) {
        undef $view_path;
    }
    $view_path ||= 'main';

    $resp = $renderer->render( $view_path, $formatter, {
        # general who am I dealing with
        app     => $app,
        user    => $session->get__user,
        session => $session,
        # action result
        suc => $suc,        #TODO - better name
        ret => $session->vol( 'ret' ),
        err => $session->vol( 'err' ),
        msg => $session->vol( 'msg' ),
        
        # action / view inputs
        view_path     => $view_path,
        view_params   => $view_params,
        action_params => $action_params,
    } );

    return ( $session->get__session_id,
             'text/html', 
             '200 OK', 
             $resp );
    
} #handle




1;

__END__

launcher -> wrapper -> start



  
