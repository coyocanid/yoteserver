#!/usr/bin/perl

#
# DemoApp test
#

use strict;
use warnings;

use lib '/home/wolf/opensource/objectstore/lib';
use lib '/home/wolf/opensource/recordstore/lib';
use lib '/home/wolf/opensource/yoteserver/lib';
use lib '/home/wolf/opensource/yoteserver/demo/lib';
use Yote::Server;

my $cfg = {
    APP_TAG         => 'unique key',
    APP_CLASS       => 'Yote::Server::DemoApp',
    BASE_DIR        => '/home/wolf/opensource/yoteserver/demo',
    DOWNLOAD_DIR    => '/home/wolf/opensource/yoteserver/demo/html/downloads',
    IMAGE_DIR       => '/home/wolf/opensource/yoteserver/demo/html/downloads',
    OBJECT_STORE    => '/home/wolf/opensource/yoteserver/demo/data',
    TEMPLATE        => '/home/wolf/opensource/yoteserver/demo/templates',
    LAUNCH_TEMPLATE => 'launcher',
    UPLOAD_DIR      => '/home/wolf/opensource/yoteserver/demo/html/downloads',
};

Yote::Server->handle_cgi( $cfg );
