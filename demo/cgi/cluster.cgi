#!/usr/bin/perl

#
# DemoApp test
#

use strict;
use warnings;

use lib '/home/wolf/opensource/objectstore/lib';
use lib '/home/wolf/opensource/recordstore/lib';
use lib '/home/wolf/opensource/yoteserver/lib';
use lib '/home/wolf/opensource/yoteserver/demo/lib';
use Yote::Server;

my $port = $ENV{SERVER_PORT};
my $host = $ENV{SERVER_NAME};
#print "Content-type: text/plain\n\n";
#print "Welcome to host '$host' port '$port'\n";
#print Data::Dumper->Dump([\%ENV]);

my $cfg      =  {
    STORE    => "/home/wolf/opensource/yoteserver/demo/cluster",
    HOST     => $host,
    PORT     => $port,
    CLUSTERS => [
      { host => "localhost", port => 81 },
      { host => "localhost", port => 82 },
      { host => "localhost", port => 83 },
      { host => "localhost", port => 84 },
      ],
};

Yote::Server::Cluster->handle_cgi( $cfg );
