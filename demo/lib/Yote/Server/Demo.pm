package Yote::Server::Demo;

#
# The moddperl test
#

use strict;
use warnings;

use lib '/home/wolf/opensource/yoteserver/lib';
use lib '/home/wolf/opensource/yoteserver/demo/lib';
use Yote::Server;

sub handler {
    my $r = shift;
    Yote::Server::handle_modperl( $r, 'Yote::Server::DemoApp', 'key' );
    return 0;
}

1;

__END__
