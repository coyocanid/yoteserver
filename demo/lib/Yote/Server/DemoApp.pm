package Yote::Server::DemoApp;

use strict;
use warnings;

use lib '/home/wolf/opensource/yoteserver/lib';
use lib '/home/wolf/opensource/yoteserver/demo/lib';

use Yote::Server::App;
use base 'Yote::Server::App';

sub _init {
    my $self = shift;
    $self->REGISTER_ACCOUNT( "root", "toot", "", 1 );
}

sub GREET {
    "NAAARRF";
}

sub upload {
    my( $self, $args, $session, $user ) = @_;

    if( $user ) {
        print STDERR "UPL\n";
        my $down_dir = $self->CFG('UPLOAD_DIR');
        my $img1 = $self->UPLOAD_IMAGE( $args, 'file1', $down_dir );
        my $img2 = $self->UPLOAD_IMAGE( $args, 'file2', $down_dir );
        
        print STDERR "UPLOAD 1 <$img1,$img2>\n";
        if( $img1 && $img2 ) {
            $user->set_img1( $img1 );
            $user->set_img2( $img2 );
        } else {
            $session->err( 'Must upload two files at once' );
            return 0;
        }
    }

} #upload

1;
