package TestBuilder;

#
# Test Yote::Server::AppBuilder
# by simulating answers to the questions.
# The answer list is passed to the constructor.
#

use strict;
use warnings;
no warnings 'uninitialized';

use Yote::Server::AppBuilder;
use base 'Yote::Server::AppBuilder';

sub new {
    my( $pkg, $answers ) = @_;
    die unless ref( $answers ) eq 'ARRAY';
    bless { answers => $answers }, $pkg;
} #new

sub getinput {
    my $self = shift;
    my $ans = $self->{answers};
    die unless @$ans;
    my $inpt = shift @$ans;
    print "\n<$inpt\n";
    $inpt eq '' ? undef : $inpt;
} #getinput

sub getpwinput {
    my( $self, $char ) = @_;
    my $ans = $self->{answers};
    die unless @$ans;
    my $inpt = shift @$ans;
    print "\n<$inpt\n";
    $inpt eq '' ? undef : $inpt;
} #getpwinput


1;
