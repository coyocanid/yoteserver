use strict;
use warnings;
no warnings 'uninitialized';

use lib 't/lib';

use Data::Dumper;
use File::Copy;
use File::Temp qw/ :mktemp tempdir /;
use File::Path qw/ remove_tree /;
use Test::More;
use Carp;
$SIG{ __DIE__ } = sub { Carp::confess( @_ ) };

BEGIN {
    use_ok( "Yote::Server::AppBuilder" ) || BAIL_OUT( "Unable to load 'Yote::Server::AppBuilder'" );
    use_ok( "TestBuilder" ) || BAIL_OUT( "Unable to load 'TestBuilder'" );
}

test_suite();

done_testing;

exit( 0 );

sub test_suite {
    my $app_root = tempdir( CLEANUP => 0 );
    my $out;
    eval {
        local( *STDOUT );
        open( STDOUT, ">>", \$out );
        my $builder = new TestBuilder( [
            'demoapp',  #app name
            "$app_root/demoapp",  #app root
            '', #user to own app root
            '', #group to own app root
            'My::DemoApp', #app class
            '', # datastore directory
            '', # datastore directory user
            '', # datastore directory group
            '', # download directory
            '', # image directory
            '', # upload directory
            '', # template directory
            '', # copy cgi 
            '', # copy modperl
            'fred', #admin
            'fred1',#mismatch pw
            'fred2',
            'fred1',#matched pw
            'fred1',
          ] );
        $builder->build;
        
        my $store = Data::ObjectStore->open_store( "$app_root/objstore" );
        ok( $store, "Store was created" );

        print "DONE\n";

        # now check if the building worked allright
        
    };
    my @out = split( /[\n\r]+/, $out );
    print join("\n",@out)."\n";
} #test_suite
